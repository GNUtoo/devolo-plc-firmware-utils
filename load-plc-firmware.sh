#!/bin/sh
# PLC firmware loader.
# Copyright (C) 2017 Denis 'GNUtoo' Carikli <GNUtoo@no-log.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

set -x

usage()
{
    echo "Usage: $0 <path/to/pib> <path/to/nvm>"
    exit 1
}

if [ $# -ne 2 ] ; then
    usage
fi

gpio_power="/sys/class/gpio/gpio13"
plc_iface="eth0"

pib="$1"
nvm="$2"

if [ -f /sys/class/net/br-lan ] ; then
  brctl delif br-lan "${plc_iface}"
fi

echo 1 > "${gpio_power}/value"

ifconfig "${plc_iface}" up

amptool -i "${plc_iface}" -Iar
ampboot -i "${plc_iface}" -P "${pib}" -N "${nvm}"
