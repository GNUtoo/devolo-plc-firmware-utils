DAK ?= `rkey secret.key -D`
NMK ?= `rkey secret.key -M`
MAC ?= f4:06:8d:00:00:00
.PHONY: all
all:
	modpib -v -M $(MAC) -N $(NMK) -D $(DAK) 2555.pib
	setpib 2555.pib 7 byte 0
	#ampboot -i pe0 -P 2555.pib -N  int6x00.nvm
