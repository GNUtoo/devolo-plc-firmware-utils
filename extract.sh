#!/bin/sh

# PLC firmware extractor.
# Copyright (C) 2017 Denis 'GNUtoo' Carikli <GNUtoo@no-log.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

usage()
{
  echo "Usage: $0 list-firmware-devices </path/to/avpromanager.bin>"
  echo "Usage: $0 list-firmware-symbols </path/to/avpromanager.bin>"
  echo "Usage: $0 extract </path/to/avpromanager.bin> <symbol> [path/to/destination]"
  exit 1
}

extract_firmware_from_symbol()
{
    binary="$1"
    symbol="$2"
    destination="$3"
    data="$(mktemp)"

    if [ -z "${destination}" ] ; then
	destination="${symbol}"
    fi

    # 1) Get the data offset
    # TODO: Column 4 is VMA, column 5 is LMA, look what is the difference
    # Here both were the same
    data_addr=$(objdump -h "${binary}"  | grep " \+\.data \+" | awk '{print $4}')

    # 2) Extract the data file
    objcopy "${binary}" /dev/null --dump-section ".data=${data}"

    # 3) get the symbol address
    symbol_addr=$(objdump -t "${binary}" | grep -i " \+${symbol}$" | awk '{print $1}')
    symbol_size=$(objdump -t "${binary}" | grep -i " \+${symbol}$" | awk '{print $5}')

    # 4) compute the offset within the section data and extract

    # Convert to decimal
    data_addr=$(printf "%d\n" 0x${data_addr})
    symbol_addr=$(printf "%d\n" 0x${symbol_addr})
    symbol_size=$(printf "%d\n" 0x${symbol_size})

    file_offset=$(expr ${symbol_addr} - ${data_addr})
    dd "if=${data}" bs=1 skip=${file_offset} count=${symbol_size} "of=${destination}"
}

list_plc_device_correspondance()
{
    binary="$1"
    strings "${binary}"  | grep "devolo .*\[.*\]" | sort
}


list_firmwares_symbols()
{
    binary="$1"
    objdump -t "${binary}" | awk '{print $NF}' | grep "_devolo_.*_pib$"
}

if [ $# -ne 2 -a $# -ne 3 -a $# -ne 4 ] ; then
  usage
elif [ $# -eq 2 -a "$1" = "list-firmware-symbols" ] ; then
    list_firmwares_symbols "$2"
elif [ $# -eq 2 -a "$1" = "list-firmware-devices" ] ; then
    list_plc_device_correspondance "$2"
elif [ $# -eq 3 -a "$1" = "extract" ] ; then
  extract_firmware_from_symbol "$2" "$3"
elif [ $# -eq 4 -a "$1" = "extract" ] ; then
  extract_firmware_from_symbol "$2" "$3" "$4"
else
  usage
fi
